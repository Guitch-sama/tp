export default interface Order {
    id: number;
    packages: Package[];
    delivery: Delivery
}

interface Delivery {
    storePickingInterval: DeliveryInterval;
    deliveryInterval: DeliveryInterval;
    contact: Contact;
    carrier: Carrier;
}

interface DeliveryInterval {
    start: string;
    end: string;
}

export interface Contact {
    fullname: string;
    email: string;
    phone: string;
    address: string;
    postalCode: string;
    city: string;
}

interface Carrier {
    name: string;
}

interface Package {
    length: Mesure;
    width: Mesure;
    height: Mesure;
    weight: Mesure;
    products: Product[];
}

interface Product {
    name: string;
    price: Monetary;
}

interface Monetary {
    currency: string;
    value: number;
}

interface Mesure {
    unit: string;
    value: number;
}

