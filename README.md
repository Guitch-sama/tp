## Question 2:
Le pattern d'architecture qui a été utilisé est un pattern Model View Controller(MVC) car il va permettre
d'avoir une meilleure conception du code et d'avoir une séparation des différentes données de la vue et 
du controlleur.


 ## Question 3
 
 Le principe du SOLID utilisé dans cette question est le I. C'est à dire la ségrégation des interfaces.
 Il va nous permettre d'avoir plusieurs petites interfaces spécifiques afin d'avoir un système décuplé et facilement
 modifiable.

 ## Question 4

 Pour cette question, il faut utiliser l'Adaptateur comme design pattern 

 ## Question 5
 Le design pattern utilisé lors de cette question est le singleton.
 Il a pour objectif de restreindre l'instanciation d'une classe à un seul objet dans notre cas, ça sera le Storage.
 Au final on a utilisé une façade 

 ## Question 6
 Diagramme de classe 6 fourni en png dans le dossier