import { Router, Request, Response } from 'express'
import Storage from '../models/Storage' 
import Adapter from './Adapter'
import Order from '../types'
 
export default class Controler {
    private str : Storage;
    constructor() {
        this.str = new Storage("orders");
     }

     public helloWorld(req: Request, res: Response):void{
        res.send('Hello World !')
     }

     public GetFavicon(req: Request, res: Response):void{
         res.status(204)
     }

     public async Orders(req: Request, res: Response):Promise<void>{
        const orders:Order[] = await this.str.getItems();
        orders.map(order => {
            order.delivery.contact = new Adapter()
        })
        res.json(orders);
    }

    public async AnotherOrders(req: Request, res: Response){
        const id = req.params.id
         const orders = await this.str.getItems()
         const result = orders.find((order: any) => order.id ===parseInt(id,10))
        if (!result) {
            res.sendStatus(404)
        }
 
        res.json(result)
    }

    public async PushOrders(req: Request, res: Response){
        const orders = await this.str.getItems()
        const alreadyExists =  orders.find((order: any) => order.id === req.body.id);
         if (alreadyExists) {
            res.sendStatus(409)
         }
         orders.push(req.body)
         await this.str.setItems(orders)
        res.sendStatus(201)
    }
 }