import {Contact} from '../types';
 
 export default class Adapter implements Contact{
    public fullname: string;
    public email: string;
    public phone: string;
    public address: string;
    public postalCode: string;
    public city: string;
 
     constructor(){
         this.fullname = 'Jean Marc'
         this.email = 'jeanmarc.panacloc@gmail.com'
         this.phone = '0612146798'
         this.address = '11 rue de la peluche'
         this.postalCode= '59560'
         this.city = 'Comines'
     }
 }