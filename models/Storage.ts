import storage from 'node-persist'
import Order from '../types'
import Adapter from '../controllers/Adapter';
import { orders } from '../data/_orders';
 
 export default class Storage {
    private type: string;

    constructor(type:string) {
        this.type = type
        storage.init().then(() => {
            storage.setItem(this.type,orders)
          })
        }

    public async getItems():Promise<any>{
        return storage.getItem(this.type); 
    }
    
    public async setItems(s :string):Promise<any>{
        return await storage.setItem(this.type,s);
    }




    /*public static getInstance(): Storage {
        if (!Storage.instance) {
            Storage.instance = new Storage();
        }
 
        return Storage.instance;
     }
     
    public async getItem(string :string){
        const orders:Order[] = await storage.getItem(string);
        orders.map(order => {
            order.delivery.contact = new Adapter()
        })
       return orders;
     }
 
    public async find(id :number){
        const orders = await storage.getItem('orders')
        return orders.find((order: any) => order.id === id)
     }
 
    public async push(s :string){
        const orders = await storage.getItem('orders')
        orders.push(s)
        return await storage.setItem('orders', orders)
    }*/
 }