import http from 'http'
import express from 'express'

export default class Server{
    port : number;
    app : express.Application;

    constructor(port:number,app:express.Application){
        this.port = this.normalizePort(port)
        this.app = app;

    }

    start() {
        //const port = this.normalizePort(process.env.PORT || 1234)
        const server = http.createServer(this.app)
        server.listen(this.port)
        server.on('error',(error) => this.onError(error))
        server.on('listening',() => this.onListening())
    }

    protected normalizePort(value: any): number {
        const port = parseInt(value, 10)
      
        if (isNaN(port)) {
          return value
        }
      
        if (port >= 0) {
          return port
        }
      
        return value
      }

      protected onError(error: any): never {
        if (error.syscall !== 'listen') {
          throw error
        }
      
        switch (error.code) {
          case 'EACCES':
            console.error(`${this.port} requires elevated privileges`)
            process.exit(1)
          case 'EADDRINUSE':
            console.error(`${this.port} is already in use`)
            process.exit(1)
          default:
            throw error
        }
      }
      
      protected onListening(): void {
        console.log(`Listening on ${this.port}`)
      }
}