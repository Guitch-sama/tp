#!/usr/bin/env node
import Server from "./server";
import App from './app'
import http from 'http'

/*const port = normalizePort(process.env.PORT || 1234)
app.set('port', port)
*/
const port = 8000;
const exportApp = new App();
const server = new Server(port,exportApp.getInstance());


server.start();
