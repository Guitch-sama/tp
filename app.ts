import express from 'express'
import path from 'path'
import cookieParser from 'cookie-parser'
import logger from 'morgan'
import storage from 'node-persist'

import indexRouter from './routes/index'
import ordersRouter from './routes/orders'

import { orders } from './data/_orders'

export default class App{
  private instance : express.Application;

  constructor(){
      this.instance = express() 
      //this.instance.set('port',port)
      this.initDefaultData()
      this.registerMiddlewares()
      this.registerRoutes()

  }

  getInstance(){
    return this.instance;
  }

  private registerMiddlewares():void{
    this.instance.use(logger('dev'))
    this.instance.use(express.json())
    this.instance.use(cookieParser())
  }
  private registerRoutes():void{
    this.instance.use('/', indexRouter)
    this.instance.use('/orders', ordersRouter)
  }
  private initDefaultData():void{
    // Init default data
    storage.init().then(() => {
    storage.setItem('orders', orders)
    this.instance.use(express.urlencoded({ extended: false }))
    this.instance.use(express.static(path.join(__dirname, 'public')))
})
  }
} 

/*const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

// Init default data
storage.init().then(() => {
  storage.setItem('orders', orders)
})

app.use('/', indexRouter)
app.use('/orders', ordersRouter)

export default app*/