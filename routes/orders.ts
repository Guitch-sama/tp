import { Router, Request, Response } from 'express'
import storage from 'node-persist'
import Controller from '../controllers/Controller'

const router = Router()
const controller = new Controller();


router.get('/', async (req: Request, res: Response) => {
  controller.Orders(req,res);
})

router.get('/:id', async (req: Request, res: Response) => {
  controller.AnotherOrders(req,res);
})

router.post('/', async (req: Request, res: Response) => {
  controller.PushOrders(req,res);
})

export default router